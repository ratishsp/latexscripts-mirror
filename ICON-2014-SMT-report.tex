%
% File acl2014.tex
%
% Contact: koller@ling.uni-potsdam.de, yusuke@nii.ac.jp
%%
%% Based on the style files for ACL-2013, which were, in turn,
%% Based on the style files for ACL-2012, which were, in turn,
%% based on the style files for ACL-2011, which were, in turn, 
%% based on the style files for ACL-2010, which were, in turn, 
%% based on the style files for ACL-IJCNLP-2009, which were, in turn,
%% based on the style files for EACL-2009 and IJCNLP-2008...

%% Based on the style files for EACL 2006 by 
%%e.agirre@ehu.es or Sergi.Balari@uab.es
%% and that of ACL 08 by Joakim Nivre and Noah Smith

\documentclass[11pt]{article}
\usepackage{acl2014}
\usepackage{times}
\usepackage{url}
\usepackage{latexsym}
\usepackage{graphicx}
\usepackage{color}
\usepackage{multirow} 
\usepackage{verbatim}
\usepackage{xunicode}
\usepackage{fontspec}
% These will call appropriate hyphenation.
\usepackage{xltxtra}

%% Write the appropriate fonts here
\font\writeMl="FreeSerif:script=mlym" at 8pt
\font\writeHi="Lohit Hindi:script=deva" at 8pt
\font\writeMr="Lohit Marathi:script=deva" at 8pt
\font\writeGu="Lohit Gujarati:script=gujr" at 8pt
%\font\writeTe="Lohit Telugu:script=telu" at 8pt
\font\writeTa="Lohit Tamil:script=taml" at 8pt
\font\writeBn="Lohit Bengali:script=beng" at 8pt


\setmainfont{Times New Roman}

%\setlength\titlebox{5cm}

% You can expand the titlebox if you need extra space
% to show all the authors. Please do not make the titlebox
% smaller than 5cm (the original size); we will check this
% in the camera-ready version and ask you to change it back.


\title{The IIT Bombay SMT System for ICON 2014 Tools Contest}

% \author{First Author \\
%   Affiliation / Address line 1 \\
%   Affiliation / Address line 2 \\
%   Affiliation / Address line 3 \\
%   {\tt email@domain} \\\And
%   Second Author \\
%   Affiliation / Address line 1 \\
%   Affiliation / Address line 2 \\
%   Affiliation / Address line 3 \\
%   {\tt email@domain} \\}

\author{Anoop Kunchukuttan, Ratish Puduppully, Rajen Chatterjee, Abhijit Mishra, Pushpak Bhattacharyya \\
Indian Institute of Technology Bombay \\
\{anoopk, ratishp, rajen, abhijitmishra, pb\}@cse.iitb.ac.in
}

\date{}

\begin{document}
\maketitle
\begin{abstract}
  In this paper, we describe our submission to the ICON 2014 Tools Contest for Machine Translation. The source languages are English, Marathi, Tamil, Telugu, Bengali and the target language is Hindi. We submitted 15 systems; 5 each for the \textit{tourism}, \textit{health} and \textit{general} domains. Our submission is a Phrase-based Statistical Machine Translation system with preprocessing and post-processing elements. As preprocessing, we perform source-side reordering for English-Hindi translation, and source-side word segmentation for Indian language to Hindi translation. The translation outputs were post-edited by transliterating untranslated words. Our goal was to handle key divergences between the language pairs involved by using language-independent methods which can be scaled across Indian languages without the need for expensive annotation creation. Hence, both the transliteration model and word-segmenter have been learnt using unsupervised techniques, whereas our source reordering works for any 
target Indian language. Our approach results in a cumulative increase of BLEU scores in range of 3-6 compared to baseline phrase based SMT systems, with source side segmentation contributing to a major chunk of improvement. This demonstrates that resources created using unsupervised methods can significantly improve SMT performance involving Indian languages.
\end{abstract}

\section{Introduction}
  \label{sec:intro}
Being one of the world's most linguistically diverse countries, the need for  machine translation (MT) and the research challenges it offers need not be emphasized. MT research for Indian languages is at a nascent stage as compared to translation involving English, European languages, Chinese, Arabic etc. The MT Tools Contest for ICON 2014 requires us to translate English, Tamil, Telugu, Bengali and Marathi into Hindi. The Indian languages (abbreviated as IL) exhibit shared characteristics like: (i) relatively free word order, with SOV being the canonical word order, (ii) similar orthographic systems descended from the Brahmi script based on auditory phonetic principles, (iii) vocabulary and grammatical tradition derived from Sanskrit, and (iv) morphological richness. Dravidian languages are highly agglutinative.

 Morphological richness of Indian languages and structural divergence between Indian languages and English demand that we look at richer methods beyond pure phrase based techniques. To handle structural divergence for English-Hindi translation, we have used a rule-based source side reordering system which works across target Indian languages. Challenges owing to morphological richness for Indian language to Hindi translation have been addressed by segmenting the source text into its morphemes prior to translation. Our results indicate that handling these key aspects, especially morphology, yields a substantial improvement in translation quality.

Due to the unavailability of linguistic resources and tools for most Indian languages, both the transliteration model and morphanalyzer have been learnt using unsupervised techniques. 

Finally, we point to the limitations of using BLEU as a metric for evaluation. Alternatively, we use METEOR for evaluation and our preliminary observations indicate that METEOR may be closer to human judgments than BLEU. This would be an interesting direction of investigation for future. We also believe that manual evaluations of the submissions should be done for any future competitions to have a more realistic evaluation and to build a pool of human judgments which can help in the study of MT evaluation  for Indian languages.

The tools and resources used in this work (transliterator \footnote{\url{https://github.com/anoopkunchukuttan/indic_nlp_resources}}, morphanalyzer \footnote{\url{https://github.com/anoopkunchukuttan/indic_nlp_library}} and source reordering system \footnote{\url{http://www.cfilt.iitb.ac.in/~moses/download/en_il_src_reorder/register.html}}) have been made available as open-source code for research use.

The rest of the paper is organized as follows: Section \ref{sec:architecture} describes our MT system's architecture while the system components - Source-Side Reordering, Source-Side Segmentation and Transliteration Post-Editing - are discussed in Section \ref{sec:components}. Section \ref{sec:configuration} discusses experimental configuration, Section \ref{sec:results} describes the results and observations while Section \ref{sec:conclusion} concludes the paper.


%\begin{figure}[h]
%\includegraphics[scale=0.5]{en-hi-in-out.jpg}
%\caption{Pipeline for En-Hi translation}
% \label{fig:en-hi-translation}
%\end{figure}

\begin{table*}[!ht]
 \begin{center}
    {%
\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
\begin{center}
\begin{tabular}{|l|c|}
\hline
\multicolumn{1}{|l|}{\textbf{Steps}} & \textbf{Sentence}\\ \hline \hline
Input Sentence &Bilirubin named colored substance is made in our body absolutely everyday . \\ \hline
Source side reordering &Bilirubin named colored substance in our body absolutely everyday made is .\\ \hline
Phrase based Translation & Bilirubin  {\writeHi नामक रंग के पदार्थ हमारे शरीर में प्रतिदिन बनते है ।}\\ \hline
Transliteration & {\writeHi वाइलीरुविन नामक रंग के पदार्थ हमारे शरीर में प्रतिदिन बनते है ।}\\ \hline
\end{tabular}
\end{center}
}%
\end{center}
  \caption{Example for En-Hi translation}
  \label{tbl:result-enhi-translation}
\end{table*}

\begin{table*}[!ht]
 \begin{center}
    {%
\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
\begin{center}
\begin{tabular}{|l|p{11.6cm}|}
\hline
\multicolumn{1}{|l|}{\textbf{Steps}} & \textbf{Sentence}\\ \hline \hline
Input Sentence &\writeMr{व्यायाम आणि पदयात्रा यापैकी एखादे वर्णित प्रकाराचे दैनिक कार्य हृदय विकार आणि मधुमेह नियंत्रित करते .} \\ \hline
Morphology Splitting &\writeMr{व्यायाम आणि पद \underline{यात्रा} यापैकी एखाद \underline{े} वर्ण \underline{ित} प्रकार \underline{ाचे} दैनिक कार्य हृदय विकार आणि मधुमेह नियंत्रित करते .}\\ \hline
Phrase based Translation & {\writeMr व्यायाम और पदयात्रा में से किसी भी वर्णित प्रकार का दैनिक कार्य हृदय रोग और मधुमेह को नियंत्रित करता है ।}\\ \hline
Transliteration & {\writeMr व्यायाम और पदयात्रा में से किसी भी वर्णित प्रकार का दैनिक कार्य हृदय रोग और मधुमेह को नियंत्रित करता है ।}\\ \hline
\end{tabular}
\end{center}
}%
\end{center}
  \caption{Example for Mr-Hi translation}
  \label{tbl:result-mrhi-translation}
\end{table*}

\section{System Architecture}
  \label{sec:architecture}
We have implemented two pipelined architectures: one for English-Hindi (en-hi) translation and another for Indian Language-Hindi (IL-hi)  translation. This section describes both these pipelines briefly. 

\subsection{English-Hindi Translation Pipeline}
An input sentence is processed through the following pipeline stages, illustrated by %Figure \ref{fig:en-hi-translation} and 
an example in Table \ref{tbl:result-enhi-translation}.

\paragraph{Source side reordering (\textbf{reord})}
The input English sentence is reordered to conform to the canonical Hindi word order by applying reordering transformations on the English parse tree, and reading off the leaves of the modified tree to generate the Hindi-ordered English (HoE) sentence.
\paragraph{Phrase-Based Machine Translation (\textbf{PB})}
The reordered English sentence is then translated using a standard phrase-based SMT (PB-SMT) model. The model has been trained on a parallel corpus of HoE to Hindi sentences.
\paragraph{Transliteration Post-Editing (\textbf{translit})}
The untranslated words (named entities and out-of-vocabulary (OOV) words) from the previous stage are transliterated to generate top-k possible transliterations. Plugging these candidate transliterations back into the translation system can generate many potential, revised translations for the source sentence. These candidates translations are rescored using a target language model to select the best final translation for the source sentence.



\subsection{Indian Language-Hindi Translation Pipeline}
An input sentence is processed through the following pipeline stages, illustrated %in Figure \ref{fig:IL-hi-translation} and 
by an example in Table \ref{tbl:result-mrhi-translation}.  

\paragraph{Source side word-segmentation (\textbf{morph})}
The input Indian language sentence is segmented using a language independent morphanalyzer. Since Indian languages are either agglutinative and/or inflectionally rich, the segmentation of the text generates simpler morphemes. The data sparsity issue is handled by this process.
\paragraph{Phrase-Based Machine Translation (\textbf{PB})}
The segmented IL sentence is then translated using a standard PB-SMT model. The model has been trained on a parallel corpus of segmented-IL to Hindi sentences.
\paragraph{Transliteration Post-Editing (\textbf{translit})}
The transliteration post-editing stage works exactly as described for the English-Hindi system described in the previous section to generate the final translation for the source sentence. For Indian languages which share vocabulary, transliteration may also serve the purpose of translation of OOV words.

%\begin{figure}[h]
%\includegraphics[scale=0.5]{IL-hi.jpg}
%\caption{Pipeline for IL-Hi translation}
% \label{fig:IL-hi-translation}
%\end{figure}



\section{System Components}
  \label{sec:components}
In this section, we describe various system components and design choices at length. 

\subsection{Source-Side Reordering}
There is a significant structural divergence between Indian languages and English. Most significant among them is that Hindi is slightly free-word order with Subject-Object-Verb (SOV) being the canonical word order, whereas English has a more rigid Subject-Verb-Object (SVO) word order. Research has shown that source side reordering to conform to target side word order improves machine translation \cite{collins2005clause}. The improvement is on account of two facts: (i) longer phrases can be learnt resulting in more fluent output, and (2) the decoder cannot look at long distance reorderings due to computational considerations, but with source-side reordering, reordering in a small window is sufficient. We use a rule based approach which applies reordering transformations to the English sentence parse tree to generate the HoE sentences \cite{AnanthakrishnanJayprasadHegde2008,patel2013reordering}. Although these rules were primarily developed for Hindi, they work for translation from English to any Indian 
languages since the the structural divergences are similar \cite{kunchukuttan2014sata}.
% The basic transformation is shown below:
% \begin{quote}
%   \resizebox{7cm}{!}{$SS_mVV_mOO_mC_m \leftrightarrow C_m'S_m'S'O_m'O'V_m'V'$}
% \end{quote}
% {\small
% \textit{where}, \\
% $S$: Subject, $O$: Object, $V$: Verb, $C_m$: Clause modifier, $X'$: Corresponding constituent in Hindi. \\
% $X$ is S, O or V \\
%    $X_m$: modifier of X
%    } \\
		
\subsection{Source-Side Word-Segmentation}
Morphologically-rich/agglutinative languages have low token-to-type ratio and can generate a potentially infinite number of word types by combining morphemes. Due to this, PB-SMT systems encounter the following challenges while learning translation systems involving morphologically rich languages: (i) unreliable estimation of word translation probabilities due to data sparsity, and (ii) difficulty in generalization to test scenarios since new word forms can come up in test irrespective of the training corpus size. To address this problem, we segment the tokens in the source sentence into its constituent morphemes. The segmented corpus is used as source side of the parallel corpus, and a test sentence is also segmented before translation. We have not done any segmentation of the target language sentence, since it introduces additional complexities in ensuring morphemes of a single target word are contiguous in the translation. Since, Hindi is not highly agglutinative, this choice is reasonable; though it 
would clearly be insufficient for morphologically rich target languages.

Most Indian languages do not have a morphanalyzer/word-segmenter. Hence, we built unsupervised morphanalyzers using \textit{Morfessor 2.0} \footnote{http://www.cis.hut.fi/projects/morpho/morfessor2.shtml}. The morphanalyzers are learnt from monolingual corpora using a probabilistic generative model which uses maximum-a-posteriori estimation with sparse priors inspired by the Minimum Description Length (MDL) principle \cite{virpioja2013morfessor}. Even though these morphanalyzers do not distinguish between stem, prefix, suffix or provide any grammatical properties, the segmentation generated by this method is sufficient for preprocessing parallel corpora. We used only the word types without considering their frequencies for training since this training configuration has been shown to perform better when no annotated data is available for tuning \cite{virpioja2013morfessor}.

% Indian languages are morphologically rich and show tendency to agglutinate. Thus they contribute to data sparsity during machine translation. We experiment with morphological splitting of source text with the intuition that this split set of words will result in less OOV words during translation. For the same we use Unsupervised Morphology Splitting as implemented in Morfessor. \footnote{http://www.cis.hut.fi/projects/morpho/morfessor2.shtml} Morfessor takes as input corpus of raw text and produces output of segmentation of word forms. Being unsupervised there is no linguistic insight needed. We first trained Morfessor on monolingual corpus of various languages. The trained models were then used to morphologically split the source text of parallel corpora for languages bn, ta, te, mr.

\subsection{Transliteration Model}
Training transliteration systems requires parallel transliteration corpora. Since such corpora is generally not available for most Indian language pairs, we utilize the unsupervised approach to machine transliteration proposed by \newcite{durrani2014integrating}. This approach requires a parallel bitext corpora, which is available when building an SMT system. The transliteration model is built in two stages. First, parallel transliteration pairs are mined from a parallel corpus. Then, a transliteration model is trained from the transliteration corpus by formulating transliteration as a translation problem from source character strings to target character strings. Most Indian language scripts, including the ones in the competition, derive from the \textit{Brahmi} script and are phonetic in nature. Moreover, the Unicode codepoints of these scripts are coordinated. A simple transliteration scheme is to just map the Unicode codepoints. We experimented with this approach too, but the statistical approach yields 
better and hence only those results are reported in the paper.

\section{Experimental Configuration}
  \label{sec:configuration}
We trained 15 systems over three domains - tourism, health and general. We use the MOSES toolkit \cite{koehn2007moses} for our PB-SMT system. We use the \textit{grow-diag-final-and heuristic} for extracting phrases and the \textit{msd-bidirectional-fe} model for lexicalized reordering. We tuned the trained models using Minimum Error Rate Training (MERT) with default parameters. For Health and Tourism, we made a training, tuning and test data split of 22800, 500, 1200 sentences respectively. For General domain, we made a training,tuning and devtest data split of 45600, 1000, 2400 sentences respectively. The official test set comprised for 500 sentences each in Health and Tourism domains and 1000 sentences in General domain for each source language. 

The transliteration pairs are mined from the parallel training corpora provided for the competition. Morphology analyzers were trained using the \textit{Leipzig Corpus} \footnote{http://corpora.uni-leipzig.de/download.html} and the monolingual data provided for the competition. We used a 5-gram language model built using SRILM \cite{stolcke2002srilm} with Kneser-Ney smoothing \cite{kneser1995improved} with 1.5 million sentences from WMT monolingual corpus.  The evaluation was done using the BLEU \cite{papineni2002bleu} and METEOR \cite{banerjee2005meteor} metrics. For METEOR, the synonyms were obtained from IndoWordNet \cite{bhattacharyya2010indowordnet}, while a variant of the IndoWordNet-assisted stemmer  \cite{bhattacharyya2014facilitating} was used for stemming. 

\section{Results and Analysis}
  \label{sec:results}
Tables \ref{tbl:result1}-\ref{tbl:result2} document the BLEU (B) and METEOR (M) scores for the systems in three domains - Tourism, Health and General.
For English-Hindi, source reordering substantially improves the translation quality and transliteration contributes to a significant increase in the translation quality.  For instance, in the General domain, BLEU scores with reordering show an increase of 3.8 points compared to PB-SMT and a cumulative increase of 4.8 BLEU points with transliteration enabled over the baseline. For Indian language-Hindi translation, source word-segmentation results in a sharp increase in the translation quality, whereas transliteration contributes to a modest increase. For Telugu-Hindi, source segmentation results in 5.6 BLEU points increase over the baseline and transliteration increments it by another 0.4 BLEU points. The METEOR scores also record a similar increase. Figure \ref{tbl:result-enhi-translation} and \ref{tbl:result-mrhi-translation} illustrate how the pre-processing and post-processing extensions help improve the baseline translation.
	
Table \ref{tbl:resulttestset} shows the BLEU and METEOR scores on the test set. The BLEU scores are lower compared to the scores obtained on the devtest dataset. On manually inspecting the translation output, we see that for many instances instead of exact n-gram match the system has chosen synonyms. BLEU does not account for such matches and hence the BLEU scores are much lower than what a manual judgment suggests. Our observations are in line with the shortcomings of BLEU as identified by \newcite{ananthakrishnan2007some}. Hence we also evaluated our systems using METEOR, which can account for synonym and stem matches. The METEOR scores on the devtest dataset and official test dataset are comparable, which we believe to be more representative of the quality of the translation system. Hence, we make the case for manual evaluation of the translation results on the official test dataset so that meaningful comparisons can be made. It will also result in a corpus of human evaluations which can be useful for the 
study of automatic evaluation metrics.
	
\begin{table*}[!ht]
 \begin{center}
    {%
\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
\begin{center}
\begin{tabular}{|p{1cm}|c|c|p{1cm}|p{1.2cm}|c|p{1cm}|p{1.2cm}|c|p{1cm}|p{1.2cm}|}
\hline
\multicolumn{2}{|c|}{} &\multicolumn{3}{|c|}{\textbf{Tourism}}&\multicolumn{3}{|c|}{\textbf{Health}}&\multicolumn{3}{|c|}{\textbf{General}}\\ \hline 
\textbf{Lang Pair} & \textbf{Metric} & \textbf{PB} & \textbf{PB+ reord} & \textbf{PB+ reord+ translit}& \textbf{PB} & \textbf{PB+ reord} & \textbf{PB+ reord+ translit}& \textbf{PB} & \textbf{PB+ reord} & \textbf{PB+ reord+ translit} \\ \hline 
% \multirow{2}{*}{\textbf{Lang Pair}} & \multirow{2}{*}{\textbf{Metric}} &\multicolumn{3}{|c|}{\textbf{Tourism}}&\multicolumn{3}{|c|}{\textbf{Health}}&\multicolumn{3}{|c|}{\textbf{General}}\\ \hline 
%  &  & \textbf{PB} & \textbf{PB+ reord} & \textbf{PB+ reord+ translit}& \textbf{PB} & \textbf{PB+ reord} & \textbf{PB+ reord+ translit}& \textbf{PB} & \textbf{PB+ reord} & \textbf{PB+ reord+ translit} \\ \hline 
\multirow{2}{*}{en-hi} &B & 20.87  & 27.22  &\textbf{28.78} &24.03 &	28.63	 &\textbf{29.3} &23.55	&28.34	&\textbf{29.37}\\ \cline{2-11}
&M &43.44  & 48.25  &\textbf{50.07} &46.83&50.38	&\textbf{51.22} &45.76 &49.90	&\textbf{51.11} \\ \hline
\end{tabular}
\end{center}
}%
\end{center}
  \caption{Tourism, Health and General domain results for en-hi}
  \label{tbl:result1}
\end{table*}

\begin{table*}[!ht]
 \begin{center}
    {%
\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
\begin{center}
\begin{tabular}{|p{1cm}|c|c|p{1cm}|p{1.2cm}|c|p{1cm}|p{1.2cm}|c|p{1cm}|p{1.2cm}|}
\hline
\multicolumn{2}{|c|}{} &\multicolumn{3}{|c|}{\textbf{Tourism}}&\multicolumn{3}{|c|}{\textbf{Health}}&\multicolumn{3}{|c|}{\textbf{General}}\\ \hline 
\textbf{Lang Pair} &\textbf{Metric} & \textbf{PB} & \textbf{PB+ morph} & \textbf{PB+ morph+ translit}& \textbf{PB} & \textbf{PB+ morph} & \textbf{PB+ morph+ translit}& \textbf{PB} & \textbf{PB+ morph} & \textbf{PB+ morph+ translit} \\ \hline 
\multirow{2}{*}{bn-hi} &B &	34.38  & 37.1	&\textbf{ 37.66} &36.46	&38.66	&\textbf{39.04} &36.24	&38.61	&\textbf{38.92}\\ \cline{2-11}
&M &55.73 &58.38 &\textbf{58.98} &57.44 &59.89	&\textbf{60.37}&57.36 &59.47	&\textbf{59.84} \\ \hline
\multirow{2}{*}{mr-hi} &B &	40.24 &\textbf{46.86}	&\textbf{46.86} &39.84	&46.86	&\textbf{46.86} &41.35	&47.92	&\textbf{47.92} \\ \cline{2-11}
&M &	60.78 &\textbf{66.47}	&\textbf{66.47} &60.29 &\textbf{66.76}	&\textbf{66.76} &61.79 &\textbf{67.17}	&\textbf{67.17}\\ \hline
\multirow{2}{*}{ta-hi}&B &	17.76 &	22.42	&\textbf{22.91} &21.55	&26.05	&\textbf{26.35} &20.45	&25.34	&\textbf{25.65}\\ \cline{2-11}
&M &	36.11 &41.61	&\textbf{42.31} &39.94&45.03	&\textbf{45.42} &38.93 &44.57	&\textbf{50.00}\\ \hline
\multirow{2}{*}{te-hi} &B &	26.99 &	31.77	&\textbf{32.45}  &29.74	&35.59	&\textbf{36.04} &29.88	&35.43	&\textbf{35.88}\\ \cline{2-11}
&M &	47.20 &52.48	&\textbf{53.35} &50.05&56.05	&\textbf{56.68}  &50.20 &55.82	&\textbf{56.38}\\ \hline
\end{tabular}
\end{center}
}%
\end{center}
  \caption{Tourism, Health and General domain results for bn,mr,ta,te - hi}
  \label{tbl:result2}
\end{table*}


%\begin{table*}[!ht]
% \begin{center}
%    {%
%\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
%\begin{center}
%\begin{tabular}{|l|c|c|c|c|}
%\hline
%\multicolumn{1}{|l|}{\textbf{Language Pair}} &\textbf{Metric} & \textbf{PB-SMT} & \textbf{PB+reord} & \textbf{PB+reord+stat-translit} \\ \hline \hline
%\multirow{2}{*}{en-hi}&B &24.03 &	28.63	 &\textbf{29.3} \\ \cline{2-5}
%&M &46.83&50.38	&\textbf{51.22} \\ \hline

%\end{tabular}
%\end{center}
%}%
%\end{center}
%  \caption{Health domain results for en-hi}
%  \label{tbl:result3}
%\end{table*}

%\begin{table*}[!ht]
% \begin{center}
%    {%
%\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
%\begin{center}
%\begin{tabular}{|l|c|c|c|c|}
%\hline
%\multicolumn{1}{|l|}{\textbf{Language Pair}} &\textbf{Metric} & \textbf{PB} & \textbf{PB+morph} & \textbf{PB+morph+translit} \\ \hline \hline
%\multirow{2}{*}{bn-hi} &B &36.46	&38.66	&\textbf{39.04}  \\ \cline{2-5}
%&M&57.44 &59.89	&\textbf{60.37} \\ \hline
%\multirow{2}{*}{mr-hi} &B&39.84	&46.86	&\textbf{46.86} \\ \cline{2-5}
%&M&60.29 &\textbf{66.76}	&\textbf{66.76} \\ \hline
%\multirow{2}{*}{ta-hi}&B &21.55	&26.05	&\textbf{26.35} \\ \cline{2-5}
%&M&39.94&45.03	&\textbf{45.42} \\ \hline
%\multirow{2}{*}{te-hi}&B &29.74	&35.59	&\textbf{36.04} \\ \cline{2-5}
%&M&50.05&56.05	&\textbf{56.68} \\ \hline

%\end{tabular}
%\end{center}
%}%
%\end{center}
%  \caption{Health domain results for bn,mr,ta,te - hi}
%  \label{tbl:result4}
%\end{table*}

%\begin{table*}[!ht]
% \begin{center}
%    {%
%\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
%\begin{center}
%\begin{tabular}{|l|c|c|c|c|}
%\hline
%\multicolumn{1}{|l|}{\textbf{Language Pair}} &\textbf{Metric} & \textbf{PB} & \textbf{PB+reord} & \textbf{PB+reord+translit} \\ \hline \hline
%\multirow{2}{*}{en-hi} &B &23.55	&28.34	&\textbf{29.37} \\ \cline{2-5}
%&M&45.76 &49.90	&\textbf{51.11} \\ \hline
%\end{tabular}
%\end{center}
%}%
%\end{center}
%  \caption{General domain results for en-hi}
%  \label{tbl:result5}
%\end{table*}

\begin{comment}

\begin{table*}[!ht]
 \begin{center}
    {%
\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
\begin{center}
\begin{tabular}{|l|c|c|c|c|}
\hline
\multicolumn{1}{|l|}{\textbf{Language Pair}}  &\textbf{Metric} & \textbf{PB} & \textbf{PB+morph} & \textbf{PB+morph+translit} \\ \hline \hline
\multirow{2}{*}{bn-hi}	&B&36.24	&38.61	&\textbf{38.92} \\ 
\cline{2-5}
&M&57.36 &59.47	&\textbf{59.84} \\ \hline
\multirow{2}{*}{mr-hi}	&B&41.35	&47.92	&\textbf{47.92} \\ 
\cline{2-5}
&M&61.79 &\textbf{67.17}	&\textbf{67.17} \\ \hline
\multirow{2}{*}{ta-hi}	&B&20.45	&25.34	&\textbf{25.65} \\ 
\cline{2-5}
&M&38.93 &44.57	&\textbf{50.00} \\ \hline
\multirow{2}{*}{te-hi}	&B&29.88	&35.43	&\textbf{35.88} \\ 
\cline{2-5}
&M&50.20 &55.82	&\textbf{56.38} \\ \hline

\end{tabular}
\end{center}
}%
\end{center}
  \caption{General domain results for bn-hi,mr-hi,ta-hi,te-hi}
  \label{tbl:result6}
\end{table*}
\end{comment}

\begin{table*}[!ht]
 \begin{center}
    {%
\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
\begin{center}
\begin{tabular}{|l|c|c|c|c|}
\hline
\multicolumn{1}{|l|}{\textbf{Language Pair}} &\textbf{Metric} & \textbf{Health} & \textbf{Tourism} & \textbf{General} \\ \hline 
\multirow{2}{*}{en-hi} &B & 19.22  & 18.35  &19.49 \\ \cline{2-5}
&M &43.71 &42.56 & 43.8\\ \hline
\multirow{2}{*}{bn-hi} &B & 28.99  & 29.16  &28.53 \\ \cline{2-5}
&M &54.59 &55.02 & 54.30\\ \hline
\multirow{2}{*}{mr-hi} &B & 36.12  & 37.05  &36.98 \\ \cline{2-5}
&M &61.69 &62.17 &62.16\\ \hline
\multirow{2}{*}{ta-hi} &B & 20.65  & 17.81  &19.31 \\ \cline{2-5}
&M &41.77 &39.95 &41.19\\ \hline
\multirow{2}{*}{te-hi} &B & 20.87  & 27.22  &28.78 \\ \cline{2-5}
&M &53.61 &49.01 &52.26\\ \hline


\end{tabular}
\end{center}
}%
\end{center}
  \caption{Evaluation scores on the official test set}
  \label{tbl:resulttestset}
\end{table*}

\subsection{Analysis of errors}
The unsupervised Morphology splitting sometimes did aggressive splitting, especially in case of named entities, hence the output translation was impacted. Example for the same is shown in Table \ref{tbl:result-aggressive-split}. The rule-based source side reordering sometimes performs reordering which results in word order not conforming to the target Hindi word order. This impacts the fluency of the resultant translation. Table \ref{tbl:result-imprecise-sourceside-reordering} gives an example.

\begin{table*}[!ht]
 \begin{center}
    {%
%\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
\begin{center}
\begin{tabular}{|l|c|}
\hline
%\multicolumn{1}{|l|}{\textbf{Steps}} & \textbf{Sentence}\\ \hline \hline
Input Sentence &{\writeMr {गोव्यामध्ये द फीस्ट ऑफ थ्री किंग्स , हॅरिटेज महोत्सव , कोंकणी नाट्य महोत्सव}}  \\ \hline
Source side reordering &{\writeMr {गोव्या मध्ये द फी स्ट ऑफ थ्री किंग \underline{्स} , हॅर \underline{िटेज} महोत्सव , कोंकण ी नाट्य महोत्सव}}\\ \hline
\end{tabular}
\end{center}
}%
\end{center}
  \caption{Example for aggressive splitting with Unsupervised morphology analyzer. Here the underlined words were incorrectly split}
  \label{tbl:result-aggressive-split}
\end{table*}

\begin{table*}[!ht]
 \begin{center}
    {%
%\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
\begin{center}
\begin{tabular}{|l|c|}
\hline
%\multicolumn{1}{|l|}{\textbf{Steps}} & \textbf{Sentence}\\ \hline \hline
Input Sentence &Burn on cooking 20 live scorpions in 1 litre sesame seed oil .  \\ \hline
Source side reordering &1 in 20 live scorpions cooking on Burn sesame seed oil litre .\\ \hline

\end{tabular}
\end{center}
}%
\end{center}
  \caption{Example for imprecise source side reordering }
  \label{tbl:result-imprecise-sourceside-reordering}
\end{table*}

% \section{Related Work}
% \label{sec:related}
% WMT 2014 \cite{bojar-EtAl:2014:W14-33} had a shared task on Machine Translation. One of the language pairs was En-Hi. The contest helped compare different systems and find the best performing system. Likewise, ICON 2014 Machine Translation tools contest is another way ahead to bring together researchers in Machine Translation with the goal of improving Machine Translation for Indian languages.
% 
% \cite{kunchukuttan2014sata} presents experiments on 11 IL to IL pairs constituting 110 translation systems. They present results with Baseline Phase based Machine Translation systems. For English, they do source-side reordering. We follow the same approach here. For handling OOV, they have a rule based transliteration engine. They exploit the coordinated Unicode ranges allocated to Indian scripts to develop a simple transliteration method that just maps the
% Unicode codepoints between scripts. In our experiments we see that unsupervised transliteration has better results compared to their technique for IL. Our transliteration technique also works for En-Hi language pair.
% 
% \textcolor{red}{Discuss Shata-anuvadak work, work on transliteration post-editing in Shata-Anuvaadak. Anything else?}

\section{Conclusion}
  \label{sec:conclusion}
We have presented results on experiments for SMT from English, Bengali, Marathi, Tamil, Telugu into Hindi. Using source side reordering, source word segmentation and transliteration results in improvement of upto 6 BLEU points over the baseline phrase-based SMT system. The key point is that these resources have been learnt using unsupervised methods, thus allowing them to be scaled to many Indian languages. We observe that BLEU is not a good indicator of translation quality, and more investigation is needed for better metrics of evaluation quality and as well as manual judgments of test output. In future, we plan to select outputs from various source reordering systems and investigate word segmentation of the target language.

\section*{Acknowledgements}
We would like to thank the National Knowledge Network for making available computational facilities on the \textit{Garuda Cloud} for performing computationally intensive tasks.

\bibliographystyle{acl}
\bibliography{ICON-2014-SMT-report}

\end{document}
